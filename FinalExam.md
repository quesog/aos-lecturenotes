---
geometry: margin=1in
---

# AOS Final Exam

# Lesson 5 - Distributed Systems

## Definitions

* Definition of a Distributed System:
  * Nodes interconnected via a LAN or WAN
  * No shared physical memory
  * Message time between nodes is not negligible to the time between events in a single process.
* "Happened Before" relationship `->`
  * If a communication event links two events, then happened before. Otherwise, concurrent.
  * ![Example](./images/lesson5/017.png)

## Lamport Clocks

* On each node, monotonic increase of own event times
* Time of message receipt event must be greater than send time
  * Cj(d) = Max(Ci(a)++, Cj)
* Concurrent events have arbitrary timestamps
* Total ordering: have a way to break the tie (e.g. smaller pid)
* Lamport Clocks for mutex lock
  * Keep a local queue, ordered by happened-before
  * To acquire lock, enqueue at local timestamp in own queue, message all other processors
  * Other processors enqueue in order in their queue and message ACK that it has been enqueued
  * Know you have the lock when top of my queue, have received ACK from others _OR_ later lock requests
  * To unlock send unlock message, other processes remove from their queues
  * Requires that messages arrive in order with no message loss
  * `3(N - 1)` messages exchanged for each lock/release
  * Can bring down to `2(N-1)` by using unlock messages as ACK
* Can ensure no anomalies as long as clock drift is less than IPC time.

## Latency Limits

* Latency: elapsed time
* Throughput: # events per unit of time (e.g. bandwidth)
* Components of RPC Latency
  1.  Client call
  2.  Controller latency
  3.  Time on wire
  4.  Interrupt handling
  5.  Server setup to execute call
  6.  Server execution and reply
  7.  Client setup to receive results and restart
* Sources of RPC latency
  1.  Marshalling and Data copying
  * Reduce by marshalling into kernel buffer directly
  * OR by shared descriptors between client stub and kernel
  3.  Control transfer
  * Start with 4 context switches
  * Reduce to 2 by overlapping with network communication
  * Reduce to 1 by spinning instead of yielding on the client
  4.  Protocol processing
  * Assume the LAN is reliable
  * Don't worry about ACK or checksumming messages
  * Don't buffer the client call - just rebuild if it fails
  * DO buffer the server response

## Active Networks

* Active network - packets carry code that are executed on the router
* ANTS (Active Node Transfer System) Toolkit
  * Application level package b/c OS change is difficult
* Keep active nodes on the edge of the network, backbone remains passive
* "Soft store" of code - Cache indexed by `type` field identifying the code to run
  * If in the store, run it
  * Otherwise, fetch it from the previous node
  * Drop capsule if not in soft store or previous node
* Pros
  * Could be useful for things like multicast
  * Flexibility from application perspective
* Cons
  * Protection threats (runtime safety, spoofing `type`s, integrity of soft store)
  * Resource management threats (flooding the internet)
* Never took off because
  * No buy-in from router vendors
  * throughput is too low at application level
  * Security
  * Psychological disgust
* Legacy lives on in SDN (software defined networking)

## Systems from Components

* Compose a network stack out of small software components
* I/O Automata - code-like specification of protocols
* OCaml - Formal semantics, compiles to machine code
* NuPrl - Theoretical framework for OCaml optimization
* Optimization Techniques
  * Explicit memory management
  * Avoiding [un]marshalling
  * Buffering parallel with transmission
  * Header compression
  * Locality enhancement for common scenarios
* Takeaways: works, but you have inefficiencies in:
  * interfacing layers/copying overhead
  * Unintended side effects (globals)
  * Cache problems

# Lesson 6 - Distributed Objects and Middleware

Not on the final

# Lesson 7 - Distributed Subsystems

## Global Memory Systems

* Use the idle memory in a cluster, trade network communication for disk I/O
* "cache" is DRAM, split into Local (my stuff) and Global (other's stuff)
* Page replacement now includes memory across the cluster - choose the oldest in the entire cluster
* Page Faults
  * Case 1 - Page fault for X on Node P
    * X is in global cache of node Q
    * Get X from Q and put in P's local cache
    * Evict P's oldest page and send to Q's global cache
    * Decrease P's global cache size
  * Case 2 - Memory pressure on node P
    * Page fault on X on P
    * Swap oldest page Y into Q's global with X
    * P has no global
  * Case 3 - Page fault for X on Node P, X not in any cache
    * Read X from disk, put in P's local cache
    * Evict P's oldest global page and send to globally oldest page in entire cluster (on host R)
    * If R's page is in global, just drop it
    * If in local, write to disk if dirty and increase global size
  * Case 4 - Fault for X on node P, actively shared with Q
    * Q has X in its local
    * Increase local on P and copy X from Q
    * Pick arbitrary page from P global and replace globally oldest page
* Summary of fault behavior:
  * ![](./images/lesson7/009.png)
* GSM does not care about shared page coherence - handle at higher level
* Age management - approximation of global LRU
  * Shift the age management between nodes
  * Break into epochs with M (max replacements) and T (max time)
  * At the beginning of each epoch, each node sends age info to initiator
  * Initiator tracks the oldest pages, calculates the minimum age
  * Sends to each node {min age, its percentage of pages that will be paged out this epoch}
  * Initiator for next epoch is node with highest weight
  * On page replacement,
    * if page age > min age discard
    * if page age < min age, send to another node
* Universal page ID (UID) [ipaddr, disk partition, inode, offset]
* PFD - Page Frame Directory - Map UID to page frame (distributed)
* GCD - Global Cache Directory - Map UID to PFD (distributed)
* POD - Page Ownership Directory - Map UID to GCD (replicated)
* Network hops - POD -> GCD -> PFD

## Distributed Shared Memory

* Automatic Parallelization - At compile time, determine where parallelization can be exploited
* Message passing parallelization - No shared data
* DSM - illusion of memory on cluster being shared
* Memory Consistency - model presented to the programmer
* Cache Coherence - how that model is implemented
* Sequential consistency
  * Typical mutex lock - programmer but not system knows about locking
* Release consistency - only enforce coherence on release
  * More efficient - don't have to cohere during critical sections
  * Overlap computation with communication
* Lazy Release Consistency (LRC)
  * Procrastination - don't cohere until acquiring the lock
  * Pull vs Push
  * Less network traffic (messages)
* Software DSM
  * At the page level
  * Partition address space and distribute ownership
  * On page fault, contact owner for location of current copy of page
  * Single writer/multiple reader protocol leads to false sharing
* LRC with multiple writer
  * Keep diff of pages on write/release
  * On acquire, fetch diffs and make consistent
  * Since lock is associated with specific area of memory, diff only reflects changes for that lock. Stops false sharing.
  * Needs garbage collection - at threshold of # diffs, apply diffs to original copies
* Alternative is Structured DSM
  * Instead of page-level, at defined data structure level

## Distributed File Systems

* Log Structured File Systems
  * write file changes to a log instead of original file
  * log segments of multiple files, write all together contiguously for performance
  * Log structured has _only_ logs, no data files
* Introducing XFS
* Dynamic Management
  * Metadata management is distributed
  * Caches in peers, not (just) centrally
* Log based striping
  * When writing changes, calculate parity
  * write across some subset of storage servers (the stripe group)
  * stripe group solves small write problem
* Cooperative Caching
  * Single Writer, multiple reader
  * Coherence at file block level
  * Writer requests write
    * Invalidate all readers
    * issue token to writer for writing
    * Token can be revoked
* Log Cleaning
  * Append-only log segments, but holes where writes later invalidated
  * Coalesce segments into new segment, invalidate old ones
  * GC them to clean up after yourself
  * Distribute the cleaning activity across the nodes
    * Both clients and servers
* Data structures
  * Distributed metadata manager to map filename to manager
  * Manager goes from
    * filedirectory -> iNumber
    * imap -> inode address
    * stripe group map -> storage server
    * index for log segment id -> stripe group map
    * -> storage servers -> data blocks
  * Client - cache locally file blocks/metadata

# Lesson 9 - Internet Computing

## Giant Scale Services

DQ Principle - `data_per_query * queries_per_second == constant`

Harvest - completeness of the answer to a request

$$D = D_v / D_f$$

* $D_v$ is available data set
* $D_f$ is the full data set

Yield - probablity of completing a request

$$Q = Q_c / Q_o$$

* $Q_c$ are requests actually completed
* $Q_o$ are requests offered

$$uptime = (MTBF - MTTR) / MTBF$$

* MTBF - mean time between failures
* MTTR - mean time to recover

Replication vs Partitioning

* If fully replicated, harvest unchanged upon failure but yield goes down
* If partitioned, harvest decreases upon failure but yield stays the same (as long as # requests doesn't increase)

Upgraydd

* _fast reboot_ - reboot all at once
* _rolling upgrade_ - one at a time
* _big flip_ - do half, then the other haf

## MapReduce

* Removes complexity of parallelism from the domain expert
* Runtime handles:
  * Intermediary file locations
  * Scoreboard of mapper/reducer assignments
  * Fault tolerance
    * Start new instances on untimely response
    * Handle redundants if both return
  * Locality management
  * Task granularity (split sizes)
  * Backup tasks

## Content Delivery Networks

Distributed Hash Table

* Hash content to (key, value) pair
  * Key is hash of content (SHA-1, 160 bit key)
  * value is node where it lives

Key should be close to a node id, but not exactly

Overlay network - virtual network on top of a physical network

* User level routing table (e.g. nodeid 60 maps to IP 192.168.1.60)
* Send message to nodeid C, but have no IP for C
  * Have IP for B, so send it to B
  * Even if B doesn't know C, will eventually reach some node that knows

Traditional DHT greedy approach - put value in node N, where N is very close to key

* metadata server overload - popular hashes congest single node
* origin server overload - all these requests show up at origin node

Enter Coral with Sloppy DHT

get/put(key) satisfied by nodes different from key. Key is just a hint.

Based on XOR distance between Nsrc and Ndestination

On lookup, go halfway (in key space) each iteration to get to the key.

If full (max key values stored) or overloaded (too many requests), back off and store at a "near" location.

# Lesson 10 - RT and Multimedia

## TS-Linux

General purpose OS favors throughput vs real-time guarantees.

Sources of latency:

* Timer latency - inaccuracy of timer mechanism - late interrupts, high granularity of timing
* Preemption latency - Interrupt occurs during a critical section that is not preemptible
* Scheduler latency - Higher priority thread prevents event being delivered

### Timer Latency

| Timer            | Pro                | Con                       |
| ---------------- | ------------------ | ------------------------- |
| Periodic         | Periodicity        | Event recognition latency |
| One-shot (exact) | timely             | overhead                  |
| Soft             | reduced overhead   | polling overhead          |
| Firm             | combines all above | N/A                       |

Soft timers = no interrupts. Just before switching back to user from kernel, check to see if there are any expired timers. (polling)

Firm timers combine soft timers with oneshot timers.

"Overshoot" knob - when in kernel anyway, if within the overshoot window, dispatch expired timers and reprogram the oneshot timer. If it doesn't happen, oneshot will clean it up.

APIC hardware makes reprogramming oneshot cheap.

### Preemption Latency

Solution - Lock breaking kernel implementation

Insert preemptible sections in the middle of long critical sections in the kernel

### Scheduler Latency

Proportional period scheduling - allocate % of CPU during _every_ time quantum

Priority scheduling - prevent priority inversion by letting subtasks assume priority of caller

## Persistent Temporal Streams

* threads - process and put data into channel with timestamp
* channel - stream of data with associated timestamps

Stream group - bundle of streams

Simple abstractions: channel, get/put

Channels are similar to UNIX sockets - unique name on network

Persistence of streams under application control

**Synchronization Causality** - Shared data modified under protection of locks.

**Temporal Causality** - Timestamp associated with output data is based on the timestamp associated with the input data.

# Lesson 8 - Failures and Recovery

## Lightweight Recoverable Virtual Memory

Persistence layer for virtual memory, using log segments similar to file system

user level library, no operating system support/dependence

Can map to one or more backing files - application chooses

| application APIs | -                               |
| ---------------- | ------------------------------- |
| init             | `initialize`                    |
|                  | `map`                           |
|                  | `unmap`                         |
| body of server   | `begin_xact(tid, restore_mode)` |
|                  | `set_range(tid, addr, size)`    |
|                  | `end_xact(tid, commit_mode)`    |
|                  | `abort_xact(tid, commit_mode)`  |
| GC               | `flush()`                       |
|                  | `truncate()`                    |

Flush and truncate are done automatically by LRVM periodically

Truncate will apply redo logs and zap the log

Redo logs - in memory LRVM structure, flushed to disk at point of commit

Optimizations

* no_restore in begin_xact
  * No in-memory undo record - unabortable
* no_flush in end_xact
  * No need to sync flush redo log, window of vulnerability

## RioVista

LRVM handles 2 types of failure:

1.  Power failure
2.  Software failure

Power failure risk is mitigated by writing redo log to disk upon commit

Battery back the memory handling the redo log, and now no need for expensive disk I/O

RIO - Battery backed file cache, `mmap` the redo log

## Quicksilver

Recovery as a first-class citizen - and distributed to boot

First to propose **Transactions** as a unifying concept for recovery management, and first to propose their use in OS.

Uses IPC via kernel service queue

Send transaction ID along with requests, and A & B agree to establish transaction link

Creates transaction tree of communication

Can delegate transaction ownership, default is creator

Transaction can be committed or aborted, either way cleanup happens

# Lesson 11 - Security

## Terminology

Privacy vs. Security

Concerns:

* Unauthorized Information release
* Unauthorized Information modification
* Unauthorized denial of use

Goal: prevent all violations -> negative statement, difficult!

## Security in the Andrew File System

Central file system, accessed by many clients

* Virtue - clients
* Venus - process running on Virtues that communicates with Vice
* Vice - file server environment

Andrew uses Private key cryptosystem. identity of user in clear text.

User username/pw just for logging in. Not sending username/pw often

Ephemeral id + keys for subsequent Venus-Vice communication

Upon login, Vice sends

* cleartoken - handshake key client (HKC)
  * Use the HKC to encrypt data
* secret token - cleartoken encrypted with key only known to Vice
  * Use this as the client identifier

RPC session establishment

* Client sends encrypted random # $X_r$ to server
* Server replies with $X_r + 1$ and a second random # $Y_r$, encrypted with HKS (same as HKC)
* Client replies to server with $Y_r + 1$ (encrypted with HKC)
* At this point, the client and server have verified that each other are genuine.
* Server sends HKS encrypted session key and starting sequence number to client

Login is special case of bind: password is HKC, username is identifier

AFS Security Report Card

| Test                           | P/F | Comment                           |
| ------------------------------ | --- | --------------------------------- |
| mutual suspicion               | P   |                                   |
| protection of system for users | F   | Users must trust system           |
| confinement of resource usage  | F   | no limiting in place, vuln to DOS |
| Authentication                 | P   |                                   |
| Server Integrity               | F   | No protection inside Vice         |
